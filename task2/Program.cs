﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        abstract class Cooking
        {
            public void ToCook()
            {
                BuyIngredients();
                CookMeel();
                EatMeal();
            }

            protected abstract void BuyIngredients();
            protected abstract void CookMeel();
            protected abstract void EatMeal();
        }

        class CookMeat : Cooking
        {
            protected override void BuyIngredients()
            {
                Console.WriteLine("Buy some meat");
            }

            protected override void CookMeel()
            {
                Console.WriteLine("Fry meat");
            }

            protected override void EatMeal()
            {
                Console.WriteLine("Eating meat");
            }
        }

        class CookSalad : Cooking 
        {
            protected override void BuyIngredients()
            {
                Console.WriteLine("Buy some ingredients for salad");
            }

            protected override void CookMeel()
            {
                Console.WriteLine("Mix ingridients");
            }

            protected override void EatMeal()
            {
                Console.WriteLine("Eating salad");
            }
        }
        static void Main(string[] args)
        {
            CookMeat cookMeat = new CookMeat();
            CookSalad cookSalad = new CookSalad();

            cookMeat.ToCook();
            Console.WriteLine(new string('-', 20));
            cookSalad.ToCook();
        }
    }
}
