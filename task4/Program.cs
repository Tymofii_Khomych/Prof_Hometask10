﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Program
    {
        class MyClass
        {
            public void CoreMethod()
            {
                Console.WriteLine("Core method");
                SituationalMethod();
            }

            protected virtual void SituationalMethod() 
            {
                Console.WriteLine("Situational method");
            }
        }

        class MyClass2 : MyClass
        {
            protected override void SituationalMethod()
            {
                Console.WriteLine("New situational method");
            }
        }

        static void Main(string[] args)
        {
            MyClass myClass = new MyClass2();
            myClass.CoreMethod();

            Console.WriteLine(new string('-', 30));

            MyClass MyNewClass = new MyClass();
            MyNewClass.CoreMethod();
        }
    }
}
